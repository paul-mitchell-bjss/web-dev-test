/*
  Test 1. Partitioning an array

  Write the code to split an incoming array into two partitions,
  the first containing all the odd numbers in the input array and the
  second containing all the even numbers im the input array.
*/

/**
 * @param [input]
 * @return []
 */
export default function partition (input) {
  return input
}
