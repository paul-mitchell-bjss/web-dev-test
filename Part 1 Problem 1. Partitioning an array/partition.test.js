import partition from './partition'

test('given empty array, returns empty array', () => {
  expect(partition([])).toEqual([])
})

test('given array with single odd number, returns partitioned result', () => {
  expect(partition([1])).toEqual([[1], []])
})

test('given array with single even number, returns partitioned result', () => {
  expect(partition([2])).toEqual([[], [2]])
})

test('given array with both odd and even numbers, returns partitioned result', () => {
  const [odds, evens] = partition([7,2,400,14,1,8,4,10,3,9,-20,0,7,2])

  expect(odds).toHaveLength(5)
  expect(odds).toEqual(expect.arrayContaining([7,1,3,9]))
  expect(evens).toHaveLength(9)
  expect(evens).toEqual(expect.arrayContaining([2,400,14,8,4,10,-20,0]))
})
