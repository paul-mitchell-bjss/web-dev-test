/*
  Test 2. Encrypting a string using the ROT13 algorithm

  ROT13 https://en.wikipedia.org/wiki/ROT13 is a simple
  reciprocal cipher for English text. Implement it such
  that the tests pass.
*/

/**
 * @param {string} input
 * @return {string}
*/
export default function rot13(input) {
  return input
}
