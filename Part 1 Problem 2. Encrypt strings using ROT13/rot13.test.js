import rot13 from './rot13'

test('given an empty string, returns an empty string', () => {
  expect(rot13('')).toEqual('')
})

test('given a plain test string, returns the enciphered string', () => {
  expect(rot13('The Quick Brown Fox Jumps Over The Lazy Dog!'))
    .toEqual('Gur Dhvpx Oebja Sbk Whzcf Bire Gur Ynml Qbt!')
})

test('given an enciphered string, returns the plain text', () => {
  expect(rot13('Gur Dhvpx Oebja Sbk Whzcf Bire Gur Ynml Qbt!'))
    .toEqual('The Quick Brown Fox Jumps Over The Lazy Dog!')
})

test('given the same string twice, returns the original', () => {
  // generate a fairly large random string
  let str = ''
  while(str.length < 1000) {
    str += String.fromCharCode(Math.floor(Math.random() * 150) + 1)
  }

  expect(rot13(rot13(str))).toEqual(str)
})
