/*
  Test 3. Extract the components from an URL string

  Decomposing an URL into its component parts is a fairly common operation.
  Write a function to implement this using any method you deem fit, including
  regular expressions. Hint: the function is only intended to be run on Node.js.

  The function should return the components as an object with the following
  property names;
    for example URL 'http://user:pass@sub.host.com:8080/p/a/t/h?query=string#hash'
      protocol   (e.g. 'http:')
      username   (e.g. 'user')
      password   (e.g. 'pass')
      hostname   (e.g. 'sub.host.com')
      port       (e.g. '8080')
      pathname   (e.g. '/p/a/t/h')
      query      (e.g. 'query=string')
      hash       (e.g. '#hash')
*/

/**
 * @param {string} url
 * @return {object}
 */
export default function parseurl(url) {
  return url
}
