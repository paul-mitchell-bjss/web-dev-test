import parseurl from './parseurl'

test('given an empty string, returns an empty object', () => {
  expect(parseurl('')).toEqual({})
})

test('given a complete URL, returns an object containing all the components', () => {
  expect(parseurl('http://user:pass@sub.host.com:8080/p/a/t/h?query=string#hash'))
    .toEqual(expect.objectContaining({
      protocol: 'http:',
      username: 'user',
      password: 'pass',
      hostname: 'sub.host.com',
      port: '8080',
      pathname: '/p/a/t/h',
      query: 'query=string',
      hash: '#hash'
    }))
})

test('given an incomplete URL, returns an object containing the available components', () => {
  expect(parseurl('https://www.google.co.uk/search?q=axolotl'))
    .toEqual(expect.objectContaining({
      protocol: 'https:',
      hostname: 'www.google.co.uk',
      pathname: '/search',
      query: 'q=axolotl'
    }))
})
